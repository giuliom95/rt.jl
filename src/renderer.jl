using StaticArrays

function readobj(filepath)
    vertices = Array{SArray{Tuple{3},Float32,1,3}, 1}()
    triangles = Array{SArray{Tuple{3},Int,1,3}, 1}()
    open(filepath) do fd
        for ln in eachline(fd)
            lnelems = split(ln)
            if length(lnelems) > 0
                @views if lnelems[1] == "v"
                    v = SVector{3}(parse.(lnelems[2:4]))
                    push!(vertices, v)
                elseif lnelems[1] == "f"
                    t = SVector{3}(parse.(lnelems[2:4]))
                    push!(triangles, t)
                end
            end
        end
    end

    vertices, triangles
end

function writeppm(image, filepath)
    w, h, cc = size(image)
    open(filepath, "w") do fd
        write(fd, "P3\n$w $h\n255\n")
        for i in h:-1:1
            for j in 1:w
                r, g, b = image[i, j, :]
                write(fd, "$r $g $b ")
            end
            write(fd, "\n")
        end
    end
end

@inline function shootray(uv, camera)
    eye, reference = camera
    eye, reference * uv
end

@inline function raytriangleintersection(ray, v1, v2, v3)
    o, d = ray
    v12 = v2 - v1
    v13 = v3 - v1
    h = cross(d, v13)
    f = 1 / dot(v12, h)
    s = o - v1
    u = f * dot(s, h)
    if (u < 0 || u > 1)
        return SVector{3, Float32}(-1, 0, 0)
    end
    q = cross(s, v12)
    v = f * dot(d, q)
    if (v < 0 || u + v > 1)
        return SVector{3, Float32}(-1, 0, 0)
    end
    t = f * dot(v13, q)
    return SVector{3, Float32}(t, u, v)
end

function rt(infile, outfile, w, h)
    image = zeros(UInt8, w, h, 3)

    vertices, triangles = readobj(infile)
    camera = SVector(0, 0, 10), SMatrix{3,3}(1, 0, 0, 0, 1, 0, 0, 0, 1)

    numtriangles = size(triangles, 2)

    basecolor = UInt8[255, 0, 0]

    @inbounds @views for i in 1:h
        for j in 1:w
            uv = normalize(SVector{3, Float32}((j / h)-.5, (i / w)-.5, -1))
            ray = shootray(uv, camera)
            
            for tri in triangles
                v1 = vertices[tri[1]]
                v2 = vertices[tri[2]]
                v3 = vertices[tri[3]]
                
                intersection = raytriangleintersection(ray, v1, v2, v3)
                
                if intersection[1] > 0
                    image[i, j, :] .= basecolor
                    break
                end
            end
        end
    end 

    writeppm(image, outfile)
end

precompile(rt, (String, String, Int, Int))
rt("in/teapot.obj", "test.ppm", 1000, 1000)  