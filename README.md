# Julia offline rendering

The main goal of this project is to code an offline rendering engine in Julia.


## History

This project has been created during Juliacon London 2018, to test if was possible to get a very simple raytracer run as fast as a C++ implementation. I managed to get it run very fast but not "C++ fast": to ray trace without shading an image bigger than 300x300 pixels of the Utah teapot, C++ is only ~10% faster.